# Flax:plugin: printableLinks

## Usage

Anyone who tried to print links got quickly lost into all those css breaking properties. And the output never got really be satisfying enough.
But there is this amazing `<wbr>` element, the not known enough `word break opportunity`. This plugin is an attempt to simplify the use ot those elements.

Using a simple regex, it will allow line break at very specific places.

## Install using npm

```bash
npm install @flaxpub/cleanlinks
```

Then, include it in your `.eleventy.js` config file:

```js
const printableLinks = require("flax-printableLinks");

module.exports = (eleventyConfig) => {
  eleventyConfig.addPlugin(printableLinks);
};
```

You can then use this filter in your files:

```
{{ content | cleanLinks | safe }}
```

## Config Options

| Option    | Type   | Default                          |
| --------- | ------ | -------------------------------- |
| selectors | string | 'a[href^="http"], a[href="www"]' |

## Config Examples

The selectors accepts any selector that can be used in a js `querySelector`.

This example will run cleanLink on all the `<blockquote>` elements.

```
{{ content | cleanLinks('blockquote') | safe }}
```

## Flax

Flax is a custom 11ty setup to publish book as website, as a screen output for a single source publishing system.
Flax is developped by the folks at the [Coko foundation](https://coko.foundation)
