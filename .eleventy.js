const jsdom = require("jsdom");
const { JSDOM } = jsdom;

module.exports = (eleventyConfig, options) => {

  eleventyConfig.addFilter("cleanLinks", function (value, selectors) {
    let linksEl = 'a[href^="http"], a[href="www"]';

    if (selectors) {
      // if options, use the selectors in the options instead of the defautl
      linksEl = selectors;
    }

    // create a jsdom per link
    const dom = new JSDOM(value);
    // get all links from the dom
    let links = dom.window.document.body.querySelectorAll(linksEl);
    //for each link,
    links.forEach((link) => {
      // do some regex replacement (TO DO: make it simpler)
      link.innerHTML = link.innerHTML
        .replace(/\/\//g, "//\u003Cwbr\u003E")
        .replace(/\,/g, ",\u003Cwbr\u003E")
        .replace(/(\/|\~|\-|\.|\,|\_|\?|\#|\%)/g, "\u003Cwbr\u003E$1")
        .replace(/\-/g, "\u003Cwbr\u003E&#x2011;");
    });
    return dom.serialize();
  })
};
